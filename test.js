const eq = require('deep-equal');

const vec =  [1,2,3,4,5,6,7,8,9,10];
const mat = vec.map(_ => vec);
const obj = {
  foo: 'one',
  bar: 'two',
  baz: 'three'
};

const c = require('./index.js');

const tests = [
  // 1 - Accumulate sum 10x10 matrix
  eq(c.accumulateSeries(mat, 1000),
     [ [ 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 ],
       [ 90, 180, 270, 360, 450, 540, 630, 720, 810, 900 ],
       [ 80, 160, 240, 320, 400, 480, 560, 640, 720, 800 ],
       [ 70, 140, 210, 280, 350, 420, 490, 560, 630, 700 ],
       [ 60, 120, 180, 240, 300, 360, 420, 480, 540, 600 ],
       [ 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 ],
       [ 40, 80, 120, 160, 200, 240, 280, 320, 360, 400 ],
       [ 30, 60, 90, 120, 150, 180, 210, 240, 270, 300 ],
       [ 20, 40, 60, 80, 100, 120, 140, 160, 180, 200 ],
       [ 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 ] ]),
  
  // 2 - accumulate sum 10x3 matrix
  eq(c.accumulateSeries(mat.slice(7), 300),
     [ [ 30, 60, 90, 120, 150, 180, 210, 240, 270, 300 ],
       [ 20, 40, 60, 80, 100, 120, 140, 160, 180, 200 ],
       [ 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 ] ]),

  // 3 - accumulate sum 10x3 matrix without normalizing
  eq(c.accumulateSeries(mat.slice(7), null),
     [ [ 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 ],
       [ 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 ],
       [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] ]),

  // 4 - accumulate sum 10x10 matrix, sort ascending
  eq(c.accumulateSeries(mat, 1000, false),
     [ [ 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 ],
       [ 20, 40, 60, 80, 100, 120, 140, 160, 180, 200 ],
       [ 30, 60, 90, 120, 150, 180, 210, 240, 270, 300 ],
       [ 40, 80, 120, 160, 200, 240, 280, 320, 360, 400 ],
       [ 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 ],
       [ 60, 120, 180, 240, 300, 360, 420, 480, 540, 600 ],
       [ 70, 140, 210, 280, 350, 420, 490, 560, 630, 700 ],
       [ 80, 160, 240, 320, 400, 480, 560, 640, 720, 800 ],
       [ 90, 180, 270, 360, 450, 540, 630, 720, 810, 900 ],
       [ 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 ] ]),

  // 5 - accumulate sum 10x10 matrix without normalization
  eq(c.cumulativeSum(mat),
     [ [ 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 ],
       [ 9, 18, 27, 36, 45, 54, 63, 72, 81, 90 ],
       [ 8, 16, 24, 32, 40, 48, 56, 64, 72, 80 ],
       [ 7, 14, 21, 28, 35, 42, 49, 56, 63, 70 ],
       [ 6, 12, 18, 24, 30, 36, 42, 48, 54, 60 ],
       [ 5, 10, 15, 20, 25, 30, 35, 40, 45, 50 ],
       [ 4, 8, 12, 16, 20, 24, 28, 32, 36, 40 ],
       [ 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 ],
       [ 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 ],
       [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] ]),

  // 6 - accumulate sum 1x10 vector
  eq(c.cumulativeSum(vec),
     [ 55, 54, 52, 49, 45, 40, 34, 27, 19, 10]),

  // 7 - transpose 10x10 matrix
  eq(c.transpose2D(mat),
     [ [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
       [ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 ],
       [ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 ],
       [ 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 ],
       [ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 ],
       [ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 ],
       [ 7, 7, 7, 7, 7, 7, 7, 7, 7, 7 ],
       [ 8, 8, 8, 8, 8, 8, 8, 8, 8, 8 ],
       [ 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 ],
       [ 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 ] ]),

  // 8 - transpose 3x10 matrix
  eq(c.transpose2D(mat.slice(7)),
                   [ [ 1,  1,  1],
                     [ 2,  2,  2],
                     [ 3,  3,  3],
                     [ 4,  4,  4],
                     [ 5,  5,  5],
                     [ 6,  6,  6],
                     [ 7,  7,  7],
                     [ 8,  8,  8],
                     [ 9,  9,  9],
                     [ 10, 10, 10] ]),

  // 9 - vector sum 1x10 vector with itself
  eq(c.vectorSum(vec, vec), vec.map(item => item * 2)),

  // 10 - dot product 1x10 vector with itselff
  eq(c.scalarProd(vec, vec), 385),

  // 11 - cross product two perpendicular vectors
  eq(c.vectorProd([2,1,0],[1,2,0]), [0,0,3]),

  // 12 - compute magnitude of 1x10 vector
  eq(c.vectorMag(vec), 19.621416870348583), // Javascript precision
  // 13 - compute magnitude of unit 1x3 vector
  eq(c.vectorMag([0,1,0]), 1),

  // 14 - compute max of 1x10 integer sequence vector
  eq(c.max(vec), 10),

  // 15 - compute max of 10x10 repeated integer sequence matrix
  eq(c.max2D(mat), 10),

  // 16 - compute sequence from 0 to 10
  eq(c.sequenceUpTo(10), [0,1,2,3,4,5,6,7,8,9]),

  // 17 - print a list of 7 zeros
  eq(c.zeros(7), [0,0,0,0,0,0,0]),

  // 18 - epoch a 1x10 vector into slices of 3
  eq(c.epochVec(vec, 3),
     [ [ 1, 2, 3 ], [ 4, 5, 6 ], [ 7, 8, 9 ], [ 10, 0, 0 ] ]),
  
  // 19 - epoch a 1x10 vector with unequal slices
  eq(c.epochVec(vec, [1, 2, 3]),
     [[1], [2, 3], [4, 5, 6], [7, 8, 9, 10]]),
  
  // 20 - epoch a 1x10 vector with 'otherwise equal' slices
  eq(c.epochVec(vec, [3, 3, 3]),
     [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10]]),

  // 21 - epoch a 1x10 vector with 'otherwise equal' slices, but go too far
  eq(c.epochVec(vec, [3, 3, 3, 3, 3]),
     [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 0, 0]]),
  
  // 22 - crossFilt a vec by odd numbers in another
  eq(c.crossFilt(vec, ["This", "didn't",
                       "should", "work",
                       "pass"],
                 (item) => ((item - 1) % 2 === 0), { returnTest: false }),
     ["This", "should", "pass"]),

  // 23 - crossFilt and keep testVec
  eq(c.crossFilt(vec, ["This", "didn't",
                       "should", "work",
                       "pass"],
                 (item) => ((item - 1) % 2 === 0)),
     [vec.filter((item) => ((item - 1) % 2 === 0)),
      ["This", "should", "pass"]]),

  // 24 - crossFilt multiple series and keep testVec
  eq(c.crossFilt(vec, [["This", "didn't",
                        "should", "work",
                        "pass"],
                       ["Also", "I",
                        "this", "failed",
                        "too"]],
                 (item) => ((item - 1) % 2 === 0)),
     [vec.filter((item) => ((item - 1) % 2 === 0)),
      ["This", "should", "pass"],
      ["Also", "this", "too"]]),

  // 25 - downsampleVec by 3
  eq(c.downsampleVec(vec, 3),
     [1, 4, 7, 10]),

  // 26 - downsampleVec by 3/2
  eq(c.downsampleRational(vec, 3, 2),
     [1, 3, 4, 5, 7, 9, 10]),

  // 27 - downsampleVec by 3/2 and trim
  eq(c.downsampleRational(vec, 3, 2, true),
     [1, 3, 4, 5, 7, 9]),

  // 28 - clamp value above 5
  eq(vec.map(val => c.clamp(val, 5, 'above')),
     [1,2,3,4,5,5,5,5,5,5]),

  // 29 - clamp value below 5
  eq(vec.map(val => c.clamp(val, 5, 'below')),
     [5,5,5,5,5,6,7,8,9,10]),

  // 30 - compareInterval with earlier-later
  eq(c.compareInterval([1, 2], [3, 4]), [false, true]),

  // 31 - compareInterval with later-earlier
  eq(c.compareInterval([3, 4], [1, 2]), [true, false]),

  // 32 - firstDifference with vec
  eq(c.firstDifference(vec),
     [1, 1, 1, 1, 1, 1, 1, 1, 1]),

  // 33 - indexSort with reversed indices for vec
  eq(c.indexSort(vec, vec.reverse().map(val => val - 1)),
     vec.reverse()),

  // 34 - vectorAccess on obj
  eq(c.vectorAccess(obj, ['foo', 'bar']),
     ['one', 'two']),

  // 35 - average of vec, unweighted
  eq(c.average(vec), 5.5),

  // 36 - average of vec, back half weighted at zero
  eq(c.average(vec, [1, 1, 1, 1, 1, 0, 0, 0, 0, 0]), 1.5),

  // 37 - compute min of 1x10 integer sequence vector
  eq(c.min(vec), 1),

  // 38 - compute min of 10x10 repeated integer sequence matrix
  eq(c.min2D(mat), 1),

  // 39 - clamp with non-numerics
  eq(c.clamp(null, 1), null),

  // 40 - sample from vec
  c.sampleVector(vec, 5).every(function(el) {
    return vec.includes(el);
  }),

  // 41 - count unique in array should have 10 elements
  eq(Object.keys(c.countUnique(vec)).length, 10),

  // 42 - count unique in array should have 1 for every value
  c.objToArray(c.countUnique(vec)).every(val => val === 1),

  // 43 - objToArray returns the values of an object
  eq(c.objToArray(obj), ['one', 'two', 'three']),

  // 44 - linVec produces tens up to 100
  eq(c.linVec(10, 100), [0, 10, 20, 30, 40, 50, 60, 70, 80, 90])
];

const failingTestInds = tests
        .map((res, ind) => res === false ? ind + 1 : null)
        .filter(Boolean);

if (failingTestInds.length > 0) {
  console.error("Test(s) number", failingTestInds,
                "failed. Call your developer.");
  throw new Error("Some tests failed");
}
