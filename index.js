/**
 * Produces a vector of linear points (equally spaced) from a lower limit to an
 * upper limit. Essentially like "linspace" for MATLAB but 1D only.
 * 
 * @param {number} numSteps - The number of steps (output elements) in the
 * array.
 * @param {[number]} high - The upper limit (open). Default is 0.
 * @param {[number]} low - The lower limit (closed). Default is 1.
 * @returns {Array} - The vector.
 */
function linVec(numSteps, high = 1, low = 0) {
  const delta = (high - low) / numSteps;
  return sequenceUpTo(numSteps).map(ind => ind * delta + low);
}

/**
 * Converts an object to an array of its values.
 *
 * @param {Object} obj - An object with a key-value stucture.
 * @returns {Array} An array of the object's values
 */
function objToArray(obj) {
  return Object.keys(obj).map(key => obj[key]);
}

/**
 * Counts the numbers of unique items in an array.
 *
 * @param {Array} array The items to count
 * @returns {Object} A dictionary organized as `item: count`.
 */
function countUnique(array) {
  let dict = {};
  array.forEach(item => {
    if (!dict.hasOwnProperty(item)) {
      dict[item] = 1;
    } else {
      dict[item]++;
    }
  });

  return dict;
}

/**
 * Returns one or more random samples from a vector (with replacement).
 * @param {Array} arr - The vector to sample.
 * @param {number} [count] - Number of samples to return. Defaults to 1.
 * @returns {Array} A vector of samples from the input vector.
 */
function sampleVector(arr, count = 1) {
  const samples = [];
  while (count > 0) {
    samples.push(arr[Math.floor((Math.random()*arr.length))]);
    count--;
  }
  return samples;
}

/**
 * Computes the weighted average of an input vector. If no weights are
 * specified, the unweighted average is computed.

 * @param {number[]} arr - The input vector. (Could be any other type that
 * implements reduce and divide on itself, and add on its elements.)
 * @param {number[]} [weights] - Vector of weights corresponding to each
 * element.
 * @returns {number} - The average.
 */
function average(arr, weights) {
  if (weights === undefined) {
    weights = zeros(arr.length).map(zero => zero + 1);
  }

  // Little tweak to allow Immutable.JS(tm) in.
  const arrayLength = arr.length || arr.size;

  return arr.reduce((avg, item, ind) => avg + item * weights[ind], 0)
    / arrayLength;
}

/**
 * Puts an array into the order of indices passed. Returns the sorted copy.
 * @param {Array} arr - The array to sort on.
 * @param {number[]} sortIndices - The new order of indices.
 * @returns {Array} - A sorted copy of the array.
 */
function indexSort(arr, sortIndices) {
  const sortedArr = [];
  sortIndices.forEach(index => sortedArr.push(arr[index]));
  return sortedArr;
}

/**
 * Accesses an object by an array of key strings.
 * @param {Object} obj - The object to access.
 * @param {string[]} keysArray - The keys to get.
 * @returns {Array} - An array of values in the order of keys given.
 */
function vectorAccess(obj, keysArray) {
  return keysArray.map(key => obj[key]);
}

/**
 * Function that computes the first difference of a vector.
 * @param {number[]} vector - The vector to operate on.
 * @returns {vector} - A vector of length one less than the input vector.
 */
function firstDifference(vector) {
  var diffVec = vector.map((val, ind) => vector[ind + 1] - val);
  diffVec.pop();

  return diffVec;
}

/**
 * Checks if a range specified by a pair of numbers is wider at either extreme
 * than another pair.
 * @param {Array} interval - An array of numbers representing paired endpoints
 * of intervals.
 * @param {Array} otherInterval - An array of numbers representing paired
 * endpoints of intervals, the same length as `interval`.
 * @returns {Array} - An array of booleans the same length as one of the
 * interval arrays, indicating whether the paired intervals exceeded the other
 * interval at either end.
 * @throws {Error} - Throws if either argument is not an array of even length. 
 */
function compareInterval(interval, otherInterval) {
  if (!(interval instanceof Array) ||
      !(otherInterval instanceof Array) ||
      (interval.length % 2 !== 0) ||
      (otherInterval.length !== interval.length)) {
    
    throw new Error("Interval arguments were not arrays of even and equal length.");
  } else {
    return interval.map((val, ind) => val <= otherInterval[ind])
      .map(boolVal => boolVal ? 1 : 0)
      .map((binVal, ind) => binVal ^ ((ind + 1) % 2))
      .map(Boolean);
  }
}

/**
 * Function for clamping values. If a value exceeds a limit, it will be set to
 * that limit instead. If the value is not of type 'number', it will just return
 * the input value.
 * @param {number} val - The value to clamp.
 * @param {number} limit - The limit the value will be compared to.
 * @param {String} limitType - If `limitType` is `'above'`, `val` will be
 * clamped if it is greater than `limit`. If `limitType` is `'below'`, `val`
 * will be clamped if it is less than `limit`.
 * @returns {number} - The clamped value.
 * @throws {Error} - Throws when `limitType` is not one of `'above'` or
 * `'below'`.
 */
function clamp(val, limit, limitType = 'above') {
  if (typeof val !== 'number') {
    return val;
  } else if (limitType === 'above') {
    return val > limit ? limit : val;
  } else if (limitType === 'below') {
    return val < limit ? limit : val;
  } else {
    throw new Error('limitType must be one of \'above\' or \'below\'.');
  }
}

/**
 * A function that filters another series/array based on a test applied to each
 * item of a test array.
 * @param {Array} testVec - The array to use for testing.
 * @param {Array} workVectors - The array (or array of arrays) to filter.
 * @param {function} testFunc - The function to use for testing. Expects an item
 * in the Array as its sole argument.
 * @param {Object} opts - Optional options.
 * @param {bool} opts.returnTest - If true (default), prepends the filtered
 * `testVec` to the output.
 */
function crossFilt(testVec, workVectors, testFunc,
                   opts = { returnTest: true }) {
  function testItem(_, ind) {
    return testFunc(testVec[ind]);
  }
  
  let outVec = [];

  // if we've been given only one array to work on
  if (!(workVectors[0] instanceof Array)) {
    outVec = workVectors.filter(testItem);
    if (opts.returnTest === true) {
      outVec = [testVec.filter(testItem), outVec];
    }
    
    // if we have an array of arrays
  } else if (workVectors[0] instanceof Array) {
    outVec = workVectors.map(vec => vec.filter(testItem));
    if (opts.returnTest === true) {
      outVec.unshift(testVec.filter(testItem));
    }
  }

  

  return outVec;
}

/**
 * Slices a vector into subvectors of equal (or specified) length based on an
 * epoch.
 * @param {Array} seriesList - A vector to slice.
  * @param {number|number[]} epochLength - The length of each slice. An array
 * argument will produce as many arrays as the array has length, of the
 * corresponding lengths (plus one for the remaining values if they exist).
 * @returns {Array} - A vector of vectors proceeding in the order given.
 * @throws {Error} - If epochLength is not a number or array of numbers.
 */
function epochVec(seriesList, epochLength) {
  let listCount = 1;
  if (typeof epochLength === 'number') {
    listCount = Math.ceil(seriesList.length / epochLength);
  } else if (epochLength instanceof Array) {
    listCount = epochLength.length + 1;
  }
  
  if (listCount < 1) {
    console.warn("Epoch length is longer than data given.");
    return seriesList;
  }
  
  return sequenceUpTo(listCount)
    .map(sliceInd => {
      var outSlice = [];
      var sliceStart = null;
      // Test to get epochLength if array
      var testLength = 0;
      if (epochLength instanceof Array) {
        // If we're out of specified lengths, we want to just get the rest.
        sliceStart = epochLength.slice(0, sliceInd).reduce(sum, 0);
        if (sliceStart === undefined) {
          sliceStart = epochLength.reduce(sum) - 1;
        }
        testLength = epochLength[sliceInd] || seriesList.length - sliceStart;
        if (testLength < 0 ) { return null; }
      } else if (typeof epochLength === 'number') {
        sliceStart = sliceInd * epochLength;
        testLength = epochLength;
      } else {
        throw new Error("epochLength should be a number or array.");
      }
      
      if (seriesList[sliceStart] !== undefined) {
        outSlice = seriesList.slice(sliceStart, sliceStart + testLength);
        // Pad if almost at end of input.
        if (outSlice.length < testLength) {
          outSlice = outSlice.concat(zeros(testLength - outSlice.length));
        }
      } else {
        // Pad if completely out of input values.
        outSlice = zeros(epochLength);
      }
      return outSlice;
    }).filter(slice => slice ? slice.length > 0 : slice);

}

/**
 * Turns an array of numeric series into a cumulative sum of arrays, increasing
 * from the first series value to the last, normalized by a maximum value.
 * @param {Array} seriesList - An array of arrays.
 * @param {number} maxVal - The maximum value to normalize to. Default 1. Does
 * not normalize if passed null.
 * @param {boolean} sortDescending - Sort resulting series descending or
 * ascending. Defaults to ascending.
 * @returns {Array} - An array of arrays.
 */
function accumulateSeries(seriesList, maxVal, sortDescending = true) {
  let mutableList = Object.assign([], seriesList);

  mutableList = sortDescending ?
    cumulativeSum(mutableList) : cumulativeSum(mutableList).reverse();

  var normalizingFactor =
        maxVal !== null ?
        maxVal / max2D(mutableList) :
        1;

  mutableList = mutableList.map((col, colInd) =>
                                col.map((el) => el * normalizingFactor));

  return mutableList;
}

/**
 * A function for downsampling a vector by a fixed integer. For example, to
 * select every third element, call `downsampleVec(array, 3)`.
 * @param {Array} array - The array to downsample.
 * @param {number} factor - The downsampling factor
 * @returns {Array} - The downsampled array.
 */
function downsampleVec(array, factor) {
  if (array.length / factor < 1) {
    console.error("Downsample factor too large. Returning empty array.");
    return [];
  } else if (factor !== Math.ceil(factor)) {
    console.error("Downsample factor is not an integer. Returning original array.");
    return array;
  } else {
    return array.filter((_, ind) => {
      return ind % factor == 0;
    });
  }
}

/**
 * A function for downsampling a vector by a rational number. For example, to
 * downsample by 1.5, call `downsampleRational(array, 3, 2)`.
 *
 * Warnings: 
 *
 * - Function does not reduce the numerator and denominator, and each of them
 * need to be smaller than the length of the array.
 *
 * @param {Array} array - The array to downsample.
 * @param {number} num - The downsampling factor's numerator.
 * @param {number} den - The downsampling factor's denominator.
 * @param {boolean} trimDown - If the downsampled array should be trimmed by 1
 * element, in the case of a downsample factor that does not evenly divide into
 * the array length.
 * @returns {Array} - The downsampled array.
 */
function downsampleRational(array, num, den, trimDown = false) {
  const dVec =
          Array.from(
            new Set(
              downsampleVec(array, num)
                .concat(downsampleVec(array, den))))
          .sort((a, b) => a > b);

  if (trimDown && dVec.length > array.length * den / num) {
    dVec.pop();
  }

  return dVec;
}

/**
 * 
 * A function for computing a cumulative sum of numbers in an array.
 * @param {Array} array - An array or vector of numbers.
 * @returns {Array} - An array with each of its previous elements summed with
 * the preceding set.
 * @throws {Error} - If the input is not 1D or 2D.
 */
function cumulativeSum(array) {
  if (array[0] instanceof Array) {
    // 2D case
    return transpose2D(array)
      .map((_, ind) => array.slice(ind))
      .filter(row => row.length > 0)
      .map(slice => slice.reduce(vectorSum));
  } else if (typeof array[0] === 'number') {
    // 1D case
    return array.map((_, ind) => array.slice(ind).reduce(sum));
  } else {
    throw new Error("Didn't get a well-formed vector or matrix to accumulate.");
  }
}

/**
 * Transposes an array of arrays so that "columns" become "rows".
 * @param {Arrays} arrayOfArrays - An array of arrays, all equal length.
 * @returns {Array} - An array of arrays, with the inner and outer dimensions
 * flipped.
 */
function transpose2D(arrayOfArrays) {
  const colLength = arrayOfArrays.length;
  const rowLength = arrayOfArrays[0].length;

  // Initialize transposed array
  // Javascript, you are ridiculous.
  var transposed = Array.from(' '.repeat(rowLength));

  return transposed.map((_, ind) => arrayOfArrays.map(col => col[ind]));
}

function sum(a,b) { return a + b; }

/**
 * A function that computes the vector sum of two vectors.
 * @param {Array} vecA - The first vector to sum.
 * @param {Array} vecB - The second vector to sum.
 * @returns {Array} - The vector sum of the two vectors.
 */
function vectorSum(vecA, vecB) { // They better be of equal length.
  if (vecB === undefined) {
    return vecA;
  }
  return vecA.map((item, ind) => item + vecB[ind]);
}

/**
 * Computes the scalar ("dot") product of two vectors.
 * @param {Array} vecA - The first vector.
 * @param {Array} vecB - The second vector.;
 * @returns {number} - The scalar product of the two vectors.
 */
function scalarProd(vecA, vecB) {
  return vecA.reduce((acc, cur, ind) => acc + cur * vecB[ind]);
}

/**
 * Computes the vector ("cross") product of two vectors. Only really works in
 * three or seven dimensions. See https://www.jstor.org/stable/2323537 for why.
 * @param {Array} vecA - The first vector.
 * @param {Array} vecB - The second vector.
 * @returns {Array} - The cross product.
 */
function vectorProd(vecA, vecB) {
  if ((vecA.length !== 3 && vecB.length !== 3) ||
      (vecA.length !== 7 && vecB.length !== 7)) {
    console.warn("Cross product only uniquely defined for 3D or 7D space.");
  }
  // from http://stackoverflow.com/a/9489115/2023432
  var outVec = [];
  for (var i in vecA) {
    outVec.push(0);
    for (var j in vecB) {
      if (j !== i) {
        for (var k in vecA) {
          if (k !== i) {
            if (k > j) {
              outVec[i] += vecA[j] * vecB[k];
            } else if (k < j) {
              outVec[i] -= vecA[j] * vecB[k];
            }
          }
        }
      }
    }
  }
  return outVec;
}

/**
 * Eagerly creates a sequence starting at zero running to max-1.
 * @param {number} max - The max value of the sequence.
 * @returns {Array} - The increasing sequence.
 */
function sequenceUpTo(max) {
  return Array.from(' '.repeat(max)).map((_, ind) => ind);
}

/**
 * Returns a vector of zeros of the specified length.
 * @param {number} len - Length of the vector
 * @returns {Array} - A vector of zeros.
 */
function zeros(len) {
  return Array.from('0'.repeat(len)).map(str => parseInt(str));
}

/**
 * Computes the magnitude ("length") of a vector.
 * @param {Array} vec - A vector whose magnitude will be computed.
 * @returns {number} - The non-negative magnitude of the vector.
 */
function vectorMag(vec) {
  return Math.sqrt(vec.reduce((acc, cur) => acc + cur * cur));
}

/**
 * Computes the minimum angle (in radians) between two vectors.
 * @param {Array} vecA - The first vector.
 * @param {Array} vecB - The second vector.
 * @returns {number} - The angle in radians.
 */
function vectorAngle(vecA, vecB) {
  return Math.acos(vectorProd(vecA, vecB) /
                   (vectorMag(vecA) * vectorMag(vecB)));
}

/**
 * Computes the maximum value of a 1D matrix.
 * @param {Array} vec - A 1D array.
 * @returns {number} - The maximum value.
 */
function max(vec) {
  return vec.reduce((cur, next) => cur > next ? cur : next, -Infinity);
}

/**
 * Computes the maximum value of a 2D matrix.
 * @param {Array} mat - A 2D array.
 * @returns {number} - The maximum value.
 */
function max2D(mat) {
  return max(mat.map(row => max(row)));
}

/**
 * Computes the minimu value of a 1D matrix.
 * @param {Array} vec - A 1D array.
 * @returns {number} - The minimum value.
 */
function min(vec) {
  return vec.reduce((cur, next) => cur < next ? cur : next, Infinity);
}

/**
 * Computes the minimum value of a 2D matrix.
 * @param {Array} mat - A 2D array.
 * @returns {number} - The minimum value.
 */
function min2D(mat) {
  return min(mat.map(row => min(row)));
}

module.exports = {
  accumulateSeries,
  average,
  clamp,
  compareInterval,
  countUnique,
  crossFilt,
  cumulativeSum,
  downsampleVec,
  downsampleRational,
  epochVec,
  firstDifference,
  indexSort,
  linVec,
  max,
  max2D,
  min,
  min2D,
  objToArray,
  sampleVector,
  scalarProd,
  sequenceUpTo,
  transpose2D,
  vectorAccess,
  vectorMag,
  vectorProd,
  vectorSum,
  zeros
};
