# vector-utils

![vector-cumulative-sum](./splash1.png)

A series of miscellaneous tools for vectorized JS operations.

## Installing

`npm install --save vector-utils`

## Docs

- Generate: `npm install; npm run docs; open docs/index.html`

## Function manifest

- accumulateSeries - Turns an array of numeric series into a cumulative sum of
  arrays, increasing from the first series value to the last, normalized by a
  maximum value.
- average - Computes the weighted or unweighted average of a vector.
- clamp - Limits a value above or below a given constant.
- compareIntervals - Checks if endpoints of pairwise intervals exceed other
  pairwise intervals.
- cumulativeSum - A function for computing a cumulative sum of numbers in an
  array.
- crossFilt - A function for filtering other vectors based on a test of one.
- countUnique - Counts the numbers of unique items in an array.
- downsampleVec - Downsamples a vector (along first dimension) by a fixed
  factor.
- downsampleRational - Downsamples a vector (along first dimension) by a
  rational number.
- epochVec - Slices a vector into a matrix of vectors of equal or specified
  length(s).
- indexSort - Sorts a vector into an arbitrary order specified by indices.
- firstDifference - Computes the first difference of a vector.
- linVec - Produces a vector of linear points (equally spaced) from a lower
  limit to an upper limit.
- max - Computes the maximum value of a 1D matrix.
- max2D - Computes the maximum value of a 2D matrix.
- min - Computes the minimum value of a 1D matrix.
- min2D - Computes the minimum value of a 2D matrix.
- objToArray - Converts an object to an array of its values.
- sampleVector - Samples elements of a vector (with replacement).
- scalarProd - Computes the scalar product of two vectors.
- sequenceUpTo - Simple integer sequences.
- transpose2D - Transposes an array of arrays so that "columns" become "rows".
- vectorAccess - Vector key access for objects.
- vectorSum - A function that computes the vector sum of two vectors.
- vectorMag - Computes the magnitude of a vector.
- vectorProd - Computes the vector product of two vectors.
- zeros - Produces a vector of zeros.
